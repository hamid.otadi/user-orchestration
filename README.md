# Work locally

Build Service

```
./gradlew clean build
```

Execute dependencies with Docker Compose and this service with Gradle:

```
docker-compose --file docker-compose.dependencies.yml build
docker-compose --file docker-compose.dependencies.yml up
./gradlew bootRun
```

Execute everything with Docker Compose

```
docker-compose --file docker-compose.dependencies.yml --file docker-compose.this.yml build
docker-compose --file docker-compose.dependencies.yml --file docker-compose.this.yml up
```

Test

```
newman run src/test/*postman_collection.json --environment src/test/veloxUserOrchestration-local.postman_environment.json --reporters cli,html --reporter-html-export newman-results.html
```

## API documentation

Online API Documentation is at:

- HTML: http://localhost:8451/user-orchestration/v1/swagger-ui.html
- JSON: http://localhost:8451/user-orchestration/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/user-orchestration/

### User-Orchestration Service images

|Version      | Description                                                                                                                                          |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1.0.0 (1.0) | Base image                                                                                                                                           |
| 1.0.1       | User-Orchestration dtos have been removed. Instead, Dtos from User Service are referenced                                                                  |
