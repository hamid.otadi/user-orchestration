package shop.velox.user_orchestration.api.controller.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import shop.velox.user_orchestration.api.controller.UserController;
import shop.velox.user.api.dto.UserDto;import shop.velox.user_orchestration.service.UserOrchestrationService;

@RestController
public class UserControllerImpl implements UserController {

  private static final Logger LOG = LoggerFactory.getLogger(UserControllerImpl.class);

  private UserOrchestrationService userOrchestrationService;

  public UserControllerImpl(
      @Autowired UserOrchestrationService userOrchestrationService) {
    this.userOrchestrationService = userOrchestrationService;
  }

  @Override
  public Mono<ResponseEntity<UserDto>> createUser(UserDto user) {

    LOG.info("user:{}", user);
    LOG.info("userOrchestrationService: {}", userOrchestrationService);
    return Mono.fromSupplier(() -> userOrchestrationService.createUser(user));
  }

  @Override
  public Mono<ResponseEntity<UserDto>> getUser(String userId) {
    return Mono.fromSupplier(() -> userOrchestrationService.getUser(userId));
  }

  @Override
  public Mono<ResponseEntity<UserDto>> updateUser(String userId, UserDto user) {
    return Mono.fromSupplier(() -> userOrchestrationService.updateUser(userId, user));
  }
}
