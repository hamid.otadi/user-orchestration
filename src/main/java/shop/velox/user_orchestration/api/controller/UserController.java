package shop.velox.user_orchestration.api.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import reactor.core.publisher.Mono;
import shop.velox.user.api.dto.UserDto;


@Tag(name = "User", description = "the User API")
@RequestMapping("/users")
public interface UserController {

  @Operation(summary = "Create new User", description = "")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "201",
              description = "User created",
              content = @Content(schema = @Schema(implementation = UserDto.class))),
          @ApiResponse(
              responseCode = "409",
              description = "An user with given Id already exists",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "422",
              description = "Mandatory data missing",
              content = @Content(schema = @Schema()))
      })
  @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  Mono<ResponseEntity<UserDto>> createUser(
      @Parameter(description = "User to insert. Cannot be empty.", required = true) @RequestBody final UserDto user);

  @Operation(summary = "Find User by code", description = "")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation",
              content = @Content(schema = @Schema(implementation = UserDto.class))),
          @ApiResponse(
              responseCode = "404",
              description = "User not found",
              content = @Content(schema = @Schema()))
      })
  @GetMapping(value = "/{id:.*}", produces = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<UserDto>> getUser(
      @Parameter(description = "Id of the User. Cannot be empty.", required = true)
      @PathVariable("id") final String userId);

  @Operation(summary = "Updates an user", description = "")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation",
              content = @Content(schema = @Schema(implementation = UserDto.class))),
          @ApiResponse(
              responseCode = "404",
              description = "User not found",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "422",
              description = "Mandatory data missing",
              content = @Content(schema = @Schema()))
      })
  @PatchMapping(value = "/{id:.*}", consumes = MediaType.APPLICATION_JSON_VALUE)
  Mono<ResponseEntity<UserDto>> updateUser(
      @Parameter(description = "Id of the User. Cannot be empty.", required = true)
      @PathVariable("id") final String userId,
      @Parameter(description = "User to update. Cannot be empty.", required = true) @RequestBody final UserDto user);

}
