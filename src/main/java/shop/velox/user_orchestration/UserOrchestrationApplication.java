package shop.velox.user_orchestration;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.RequestScope;
import shop.velox.commons.rest.filters.RestTemplateHeaderModifierInterceptor;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class UserOrchestrationApplication {

  private static final Logger LOG = LoggerFactory.getLogger(UserOrchestrationApplication.class);

  @Autowired
  private PropagatingResponseErrorHandler propagatingResponseErrorHandler;

  @Autowired
  private RestTemplateHeaderModifierInterceptor restTemplateHeaderModifierInterceptor;

  public static void main(String[] args) {
    SpringApplication.run(UserOrchestrationApplication.class, args);
  }

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    RestTemplate restTemplate = builder
        .requestFactory(HttpComponentsClientHttpRequestFactory.class)
        .additionalInterceptors(restTemplateHeaderModifierInterceptor)
        .build();
    restTemplate.setErrorHandler(propagatingResponseErrorHandler);
    return restTemplate;
  }

  /**
   * This specialized version of the Spring RestTemplate supports forwarding the authorization token
   * to the target service for the request. If the current session is not authenticated, no token
   * will be used.
   */
  @Bean
  @RequestScope
  public RestTemplate keycloakRestTemplate(HttpServletRequest inReq) {
// retrieve the auth header from incoming request
    final String authHeader =
        inReq.getHeader(HttpHeaders.AUTHORIZATION);
    final RestTemplate restTemplate = new RestTemplate();
    // add a token if an incoming auth header exists, only
    if (authHeader != null && !authHeader.isEmpty()) {
      // since the header should be added to each outgoing request,
      // add an interceptor that handles this.
      restTemplate.getInterceptors().add(
          (outReq, bytes, clientHttpReqExec) -> {
            outReq.getHeaders().set(
                HttpHeaders.AUTHORIZATION, authHeader
            );
            return clientHttpReqExec.execute(outReq, bytes);
          });
    }
    return restTemplate;
  }

  @Bean
  public ServerCodecConfigurer serverCodecConfigurer() {
    return ServerCodecConfigurer.create();
  }

}