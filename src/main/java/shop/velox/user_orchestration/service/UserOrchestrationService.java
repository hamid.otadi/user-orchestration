package shop.velox.user_orchestration.service;

import org.springframework.http.ResponseEntity;
import shop.velox.user.api.dto.UserDto;
public interface UserOrchestrationService {

  /**
   * Creates a user and returns it
   *
   * @param user the user to create
   * @return the just created user
   */

  ResponseEntity<UserDto> createUser(final UserDto user);

  /**
   * Gets a user by its id
   *
   * @param userId the id of the user
   * @return the user wrapped in a {@link ResponseEntity}
   */

  ResponseEntity<UserDto> getUser(final String userId);

  /**
   * Updates a user.
   *
   * @param userId
   * @param user   contains the user information
   * @return the user wrapped in a {@link ResponseEntity}
   */

  ResponseEntity<UserDto> updateUser(String userId, UserDto user);
}