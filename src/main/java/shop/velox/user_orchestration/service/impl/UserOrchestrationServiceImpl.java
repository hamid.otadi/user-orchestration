package shop.velox.user_orchestration.service.impl;


import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.user.api.dto.UserDto;
import shop.velox.user_orchestration.service.UserOrchestrationService;

@Service
public class UserOrchestrationServiceImpl implements UserOrchestrationService {

  private static final Logger LOG = LoggerFactory.getLogger(UserOrchestrationServiceImpl.class);

  private final RestTemplate restTemplate;

  @Value("${user.url}")
  private String userUrl;


  private ResponseEntity<UserDto> userDto;

  public UserOrchestrationServiceImpl(@Autowired final RestTemplate restTemplate) {

    this.restTemplate = restTemplate;
  }

  @Override
  public ResponseEntity<UserDto> createUser(UserDto user) {
    URI uri = UriComponentsBuilder.fromUriString(userUrl)
        .path("/users")
        .build()
        .toUri();
    LOG.info("uri: {}", uri);
    ResponseEntity<UserDto> cartEntity = restTemplate.postForEntity(uri, user, UserDto.class);
    LOG.info("status: {}", cartEntity.getStatusCode());
    return ResponseEntity.status(cartEntity.getStatusCode()).body(cartEntity.getBody());
  }

  @Override
  public ResponseEntity<UserDto> getUser(String userId) {
    URI uri = UriComponentsBuilder.fromUriString(userUrl)
        .path("/users/")
        .path(userId)
        .build()
        .toUri();
    ResponseEntity<UserDto> userDto = restTemplate.getForEntity(uri, UserDto.class);
    LOG.debug("getUser status: {},  body: {}", userDto.getStatusCode(),
        userDto.getBody());
    return userDto;

  }

  @Override
  public ResponseEntity<UserDto> updateUser(String userId, UserDto user) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(userUrl)
        .path("/users/")
        .path(userId);
    URI targetUrl = uriComponentsBuilder
        .build()
        .toUri();

    ResponseEntity<UserDto> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.PATCH,
        new HttpEntity<>(user), new ParameterizedTypeReference<>() {
        });
    LOG.debug("{}, {}", responseEntity.getStatusCode(), responseEntity.getBody());

    return responseEntity;
  }
}
