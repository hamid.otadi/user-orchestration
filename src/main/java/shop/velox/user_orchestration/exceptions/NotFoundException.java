package shop.velox.user_orchestration.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response.Status;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Used to indicate that requested resource does not exist
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Resource not found")
public class NotFoundException extends ClientErrorException {

  private static final Status STATUS = Status.NOT_FOUND;

  public NotFoundException() {
    super(STATUS);
  }

  public NotFoundException(final String message) {
    super(message, STATUS);
  }
}
